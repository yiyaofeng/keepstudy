package com.keepstudy.core.date.exception;

/**
 * 异常
 * @Author: yyf
 * @Date: 2021/11/17 9:31
 * @Version: 1.0
 */
public class DateException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public DateException(Throwable e) {
        super(e);
    }

    public DateException(String message) {
        super(message);
    }

    public DateException(String message, Throwable cause) {
        super(message, cause);
    }
}
